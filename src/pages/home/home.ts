import { Util } from './../../app/Util';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { DetailsPage } from '../details/details';
//import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

@Injectable()
export class HomePage {
  public carList;
  public rootList;
  private API_URL = 'http://fipeapi.appspot.com/api/1/carros/marcas.json';
  //private API_URL = 'https://parallelum.com.br/fipe/api/v1/carros/marcas';
  
  //private API_DET_URL = 'http://fipeapi.appspot.com/api/1/carros/veiculos';

  //private API_DET_URL = 'https://parallelum.com.br/fipe/api/v1/carros/marcas/';

  constructor(public navCtrl: NavController, private http: Http) {
    this.rootList = this.getWS();
    this.initializeItems();
  }

  private initializeItems() {
    this.carList = this.rootList;
  }

  public filterList(e) {
    const search = e.target.value;
    this.initializeItems();

    if (search && search.trim() != '') {
      this.carList = this.carList.filter((item) => {
        return (item.name.toLowerCase().indexOf(search.toLowerCase()) > -1);
      });
    }
  }


  private getWS() {
    var result = [];

    this.http.get(this.API_URL).subscribe(
      data => {
        try {
          if (data) {
            let array = Util.bucketSort(JSON.parse(data['_body']), 'name');
            array.forEach(function (item) {
                let imageURL = "../../../assets/imgs/"+(item.name.replace(' ', '-')).toLowerCase()+".png";
                let errorURL = "../../../assets/imgs/error.jpg";
                item.name = item.name.toUpperCase();
                item.imageURL = Util.fileExists(imageURL)? imageURL:errorURL;

                result.push(item);
            });
          }
        } catch(e) {
          console.log(e);
        }
      }, error => {
        console.log("Erro ao consultar WebService");
        return false;
      },() => { //finish load
        console.log("sucesso");
      });
    return result;
  }

  detailsPage(id, label, imageURL) {
    this.navCtrl.push(DetailsPage, {"id": id, "label": label, "imageURL": imageURL});
  }
}
