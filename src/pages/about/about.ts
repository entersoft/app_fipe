import { Component,ViewChild } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Util } from './../../app/Util';
import { Http } from '@angular/http';


//npm install ionic-select-searchable --save

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {
	private API_URL = 'http://fipeapi.appspot.com/api/1/carros/marcas.json';
	private URL_BASE = '/fipe';
	private lastURL;
  @ViewChild('myselect') selectComponent: SelectSearchableComponent;
	marca = null;
	modelo = null;
	ano = null;

	marcaIds = [];
	modeloIds = [];
  anoIds = [];

	marcas = [{}];
	modelos = [];
	anos = [];

	public details = {};

	private loader;

	public labelModelo = "";
	public modeloIsDisabled = true;

	public labelAno = "";
	public anoIsDisabled = true;

	public labelButton = "Consultar";
	public submitIsDisabled = true;

	public cardDisabled = true;
	public cardLabel;

	constructor(
		public navCtrl: NavController, 
		private toastCtrl: ToastController, 
		private http: Http,
		private loadingCtrl: LoadingController) {
		this.initialize();

		this.loader = this.loadingCtrl.create({
				spinner: "crescent",
				content: "Consultando...",
				//duration: 3000
		});
	}

	initialize() {
		let result = [];
    this.http.get(this.API_URL).subscribe(
      data => {
          if (data) {
            let array = Util.bucketSort(JSON.parse(data['_body']), 'name');
            array.forEach(function (item) {
                //let imageURL = "../../../assets/imgs/"+(item.name.replace(' ', '-')).toLowerCase()+".png";
                //let errorURL = "../../../assets/imgs/error.jpg";
                item.name = item.name.toUpperCase();
                //item.imageURL = Util.fileExists(imageURL)? imageURL:errorURL;
                result.push({
									id: item.id,
									name: item.name
								});
            });
          }
			}, error => {
				console.log("error");
      },() => {
        this.marcas = result;
      });
		}
	
	changeModels(event: {component: SelectSearchableComponent, value: any}) {
		this.ano = null;
		this.modelo = null;

		this.anos = null;
		this.modelos = null;

		this.labelModelo = "Carregando...";

		let result = [];
		this.lastURL = this.URL_BASE+'?veiculo=carro/'+this.marca.id+'/modelos';
    this.http.get(this.lastURL).subscribe(
      data => {
          if (data) {
            let array = Util.bucketSort(JSON.parse(data['_body']), 'Label');
            array.forEach(function (item) {
                //let imageURL = "../../../assets/imgs/"+(item.name.replace(' ', '-')).toLowerCase()+".png";
                //let errorURL = "../../../assets/imgs/error.jpg";
                item.Label = item.Label.toUpperCase();
                //item.imageURL = Util.fileExists(imageURL)? imageURL:errorURL;
                result.push({
									id: item.Value,
									name: item.Label
								});
            });
          }
			}, error => {
				console.log("error");
      },() => {
				this.modelos = [{}];
				this.modelos = result;
				
				this.labelModelo = "Selecione o modelo";
				this.modeloIsDisabled = false;
						
      });
	}
	
	changeYears(event: {component: SelectSearchableComponent, value: any}) {
		this.labelAno = "Carregando...";
		
		let result = [];
    this.http.get(this.URL_BASE+'?veiculo=carro/'+this.marca.id+'/'+event.value.id).subscribe(
      data => {
          if (data) {
						console.log(data);
            let array = Util.bucketSort(JSON.parse(data['_body']), 'Label');
            array.forEach(function (item) {
                //let imageURL = "../../../assets/imgs/"+(item.name.replace(' ', '-')).toLowerCase()+".png";
                //let errorURL = "../../../assets/imgs/error.jpg";
                item.Label = item.Label.toUpperCase();
                //item.imageURL = Util.fileExists(imageURL)? imageURL:errorURL;
                result.push({
									id: item.Value,
									name: item.Label
								});
            });
          }
			}, error => {
				console.log("error");
      },() => {
				this.anos = [{}];
				this.anos = result;
				
				this.labelAno = "Selecione o ano";
				this.anoIsDisabled = false;
      });
	}

	enableSubmit(event: {component: SelectSearchableComponent, value: any}) {
		if (event.value != null) {
			//this.labelButton = "Consultar";
			this.submitIsDisabled = false;
		}
	}
 
	submit() {
		this.loader.present();

		let result = {};
    this.http.get(this.URL_BASE+'?veiculo=carro/'+this.marca.id+'/'+this.modelo.id+'/'+this.ano.id).subscribe(
      data => {
          result = JSON.parse(data['_body']);
			}, error => {
				console.log("error");
      },() => {
				this.details = result;

				this.cardDisabled = false;
				this.loader.dismiss();
      });
	}
}


